#!/usr/bin/env python3
import json
from urllib.request import urlopen, urlretrieve
# gbenz id = 193798469
user_name = 'grantbenz'
insta_url = "https://www.instagram.com/" + user_name  + "/?__a=1"
response = urlopen(insta_url)
data = response.read()
values = json.loads(data)
user_id = values['graphql']['user']['id']
print('user id: ' + user_id)
graphql_url = "https://www.instagram.com/graphql/query/?query_hash=472f257a40c653c64c666ce877d59d2b&variables={%22id%22:%22" + str(user_id) + "%22,%22first%22:%2250%22}"
print('graphql url: ' + graphql_url)
response = urlopen(graphql_url)
data = response.read()
values = json.loads(data)
# graphql is used for the __a=1 version, data is used for the graphql version
# edges = values['graphql']['user']['edge_owner_to_timeline_media']['edges']
edges = values['data']['user']['edge_owner_to_timeline_media']['edges']
print('count of edges', len(edges))
for edge in edges:
    # thumbnail src is useful for square pics but we want the whole pic here
    # image_url = edge['node']['thumbnail_src']
    image_url = edge['node']['display_url']
    image_id = edge['node']['id']
    urlretrieve(image_url,str(image_id)+'.jpg')
