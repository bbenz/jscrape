#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask import Flask
from flask import request
import mysql.connector
import hashlib
from mysql.connector.cursor import MySQLCursorPrepared
app = Flask(__name__)

db = mysql.connector.connect(
    host='localhost',
    user='root',
    passwd='root',
    database='jpod'
)
conn = db.cursor(cursor_class=MySQLCursorPrepared)

@app.route('/')
def hello_world():
    return 'Hello world'

@app.route('/lesson')
def lesson():
    sql = '''
        SELECT 
            season,
            lesson_num,
            inner_text,
            href,
            title
        FROM lesson l
        LEFT JOIN download_center dc ON l.id = dc.lesson_id
        WHERE season = 'Upper Intermediate Season 2'
        AND inner_text = 'Lesson Audio'
        -- AND inner_text = 'Dialog'
    '''
    conn.execute(sql)
    result = conn.fetchall()
    html = ''
    for row in result:
        html += '<a href="' + row[3].decode('utf-8') + '">'
        html += str(row[1]) + ' ' + row[2].decode('utf-8') + ' ' + row[4].decode('utf-8')
        html += '</a>'
        html += '<hr>'
    return html

@app.route('/myrtk')
def rtk():
    sql = '''
        SELECT id, keyword, kanji, myStory
        FROM nihongoShark.note
    '''
    conn.execute(sql)
    result = conn.fetchall()
    html = ''
    for row in result:
        html += '<h2>' + row[1].decode('utf-8') + ': ' + row[2].decode('utf-8') + '</h2>'
        html += '<div>' + row[3].decode('utf-8') + '</div>'
    return html



@app.route('/submit', methods = ['POST'])
def submit():
    data = request.get_json()
    sql = '''
        INSERT IGNORE INTO lesson 
            (season, lesson_num, title, subtitle, pathname)
        VALUES 
            (?, ?, ?, ?, ?)
        '''
    values = (data['season'], data['lessonNo'], data['title'], data['subtitle'], data['pathname'])    
    conn.execute(sql, values)
    sql = '''
        SELECT id
        FROM lesson
        WHERE season = ?
        AND lesson_num = ?
        AND title = ?
        AND subtitle = ?
        AND pathname = ?
    '''
    conn.execute(sql, values)
    row = conn.fetchone()
    id = row[0]

    sql = 'INSERT IGNORE INTO download_center (lesson_id, inner_text, href) VALUES (?, ?, ?)'
    for row in data['download-center']:
        values = (id, row['innerText'], row['href'])
        conn.execute(sql, values)

    sql = '''
        INSERT IGNORE INTO dialogue
        (lesson_id, seq, sound, expression, expr_md5, hiragana, english, speaker)
        VALUES
        (?, ?, ?, ?, ?, ?, ?, ?);
    '''
    conn.execute('DELETE FROM dialogue WHERE lesson_id = ?', (id,))
    for row in data['dialogue']:
        hash = hashlib.md5(row['kanji'].encode('utf-8')).hexdigest()
        values = (id, row['row'], row['sound'], row['kanji'], hash, row['hiragana'], row['english'], row['speaker'])
        conn.execute(sql, values)

    sql = 'INSERT IGNORE INTO pdf (lesson_id, name, URI) VALUES (?, ?, ?)'
    for row in data['pdfs']:
        values = (id, row['name'], row['URI'])
        conn.execute(sql, values)


    db.commit()
    return 'got data!'

app.run(debug=True)
