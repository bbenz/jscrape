console.log("Chrome extension go!");
// this will auto run on load
scrape();


chrome.runtime.onMessage.addListener(scrape);

function scrape(message, sender, sendResponse) {
  data = {}

  // color programs purple at start of scrape to indicate "working"
  pColor = '#FF00FF'; // purple
  let paragraphs = document.getElementsByTagName('p');
  for (element of paragraphs) {
    element.style['background-color'] = pColor;
  }

  let title =  document.querySelector('.r101-headline__cell-a > h1');
  data['title'] = (title) ? title.innerText.trim() : '';
  let subtitle = document.querySelector('.r101-headline__cell-a > p');
  data['subtitle'] = (subtitle) ? subtitle.innerText.trim() : '';
  let season = document.querySelector('.r101-headline__current > a.r101-headline__collection');
  data['season'] = (season) ? season.innerText.trim() : '';
  lessonDropdown = document.querySelector('.js-lsn3-collection-nav');
  data['lessonNo'] = lessonDropdown.options[lessonDropdown.selectedIndex].text.match(/\d+/g)[0];
  data['pathname'] = window.location.pathname;


  // lesson audio, dialog, review
  data['download-center'] = []
  aList = document.querySelectorAll('#download-center a');
  for (let i = 0; i < aList.length; i++) {
    data['download-center'].push({
      'innerText': aList[i].innerText,
      'href': aList[i].href,
    });
  }

  data['pdfs'] = []
  pdfList = document.querySelectorAll('#pdfs a');
  for (let i = 0; i < pdfList.length; i++) {
    console.log(pdfList[i].innerText);
    name = pdfList[i].innerText;
    // Have to buy subscription to download more than 10 pdfs... but this totally works
    // if (name !== "Checklist") {
    //   pdfList[i].click();
    //   console.log('clicked!');
    // }
    uri = pdfList[i].getAttribute('data-trackurl');
    data['pdfs'].push({
      'name': name,
      'URI': uri,
    });
  }

  // get dialog
  jpnDialogueList = document.querySelector('#dialogue_panel_00 tbody');
  engDialogueList = document.querySelector('#dialogue_panel_01 tbody');
  hgnDialogueList = document.querySelector('#dialogue_panel_03 tbody');
  data['dialogue'] = [];
  if (jpnDialogueList){
    for (let i = 0; i < jpnDialogueList.rows.length; i++) {
      let jpnRow = jpnDialogueList.rows[i];
      let engRow = engDialogueList.rows[i];
      let hgnRow = (hgnDialogueList) ? hgnDialogueList.rows[i] : '';
      jpnText = jpnRow.querySelector('.lsn3-lesson-dialogue__td--text');
      jpnText = (jpnText) ? jpnText.innerText.trim() : '';
      jpnSound = jpnRow.querySelector('.lsn3-lesson-dialogue__td--play button');
      jpnSound = (jpnSound) ? jpnSound.getAttribute('data-src') : '';
      engText = null;
      if (engRow) {
        engText = engRow.querySelector('.lsn3-lesson-dialogue__td--text');
      }
      engText = (engText) ? engText.innerText.trim() : '';
      speaker = jpnRow.querySelector('.lsn3-lesson-dialogue__td--name');
      speaker = (speaker) ? speaker.innerText.trim() : '';
      hiragana = null;
      if (hgnRow) {
        hiragana = hgnRow.querySelector('.lsn3-lesson-dialogue__td--text');
      }
      hiragana = (hiragana) ? hiragana.innerText.trim() : '';
      data['dialogue'].push({
        'row': i,
        'sound': jpnSound,
        'kanji': jpnText,
        'hiragana': hiragana,
        'english': engText,
        'speaker': speaker
      });
    }
  }

  // get vocab
  data['vocab'] = [];
  vocabSection = document.querySelector('#lsn3_vocabulary_section tbody');
  // first row[0] is not vocabulary at all
  for (let i = 1; i < vocabSection.rows.length; i++){
    thisRow = vocabSection.rows[i];
    // fullspeed audio
    audio_button = thisRow.querySelector('.lsn3-lesson-vocabulary__td--play button');
    audio = (audio_button) ? audio_button.getAttribute('data-src') : '';
    // breakdown audio
    audio_breakdown_button = thisRow.querySelector('.lsn3-lesson-vocabulary__td--play05 button');
    audio_breakdown = (audio_breakdown_button) ? audio_breakdown_button.getAttribute('data-src') : '';
    // get text things
    text_box = thisRow.querySelector('.lsn3-lesson-vocabulary__td--text .lsn3-lesson-vocabulary__term');
    if (!text_box){
      continue;
    }
    expression_raw = text_box.querySelectorAll('span')[0];
    expression = (expression_raw) ? expression_raw.innerText.trim() : '';
    reading_raw = text_box.querySelector('.lsn3-lesson-vocabulary__pronunciation');
    reading = (reading_raw) ? reading_raw.innerText.trim() : '';
    romaji_raw = text_box.querySelector('.lsn3-lesson-vocabulary__transcription .lsn3-lesson-vocabulary__roma');
    romaji = (romaji_raw) ? romaji.innerText.trim() : '';
    meaning_raw = text_box.querySelector('.lsn3-lesson-vocabulary__definition');
    meaning = (meaning_raw) ? meaning_raw.innerText.trim() : '';
    data['vocab'].push({
      'row': i,
      'audio': audio,
      'audio_breakdown': audio_breakdown,
      'expression': expression,
      'reading': reading,
      'romaji': romaji,
      'meaning': meaning,
    });
  }
  console.log(data['vocab']);

  // get sample sentences
  cdnRoot = 'https://cdn.innovativelanguage.com/japanesepod101'
  sampleSentenceList = document.querySelectorAll('.lsn3-lesson-vocabulary__sample tbody');
  data['sample-sentence'] = [];
  for (i = 0; i < sampleSentenceList.length; i++){
    this_tbody = sampleSentenceList[i];
    for (let j = 0; j < this_tbody.rows.length; j++) {
      jpnSound = this_tbody.rows[j].querySelector('.js-lsn3-play-vocabulary')
      jpnSound = (jpnSound) ? cdnRoot + jpnSound.getAttribute('data-src') : '';
      jpnText = this_tbody.rows[j].querySelector('.lsn3-lesson-vocabulary__term');
      jpnText = (jpnText) ? jpnText.innerText.trim() : '';
      j++;
      // get the romaji
      romaji = this_tbody.rows[j].querySelector('.lsn3-lesson-vocabulary__romanization');
      romaji = (romaji) ? romaji.innerText.trim() : '';
      j++;
      // get the english
      if (this_tbody.rows[j]) {
        engSound = (this_tbody.rows[j].querySelector('.js-lsn3-play-vocabulary')) ? this_tbody.rows[j].querySelector('.js-lsn3-play-vocabulary') : '';
        engSound = (engSound) ? cdnRoot + engSound.getAttribute('data-src') : '';
        engText = this_tbody.rows[j].querySelector('.lsn3-lesson-vocabulary__definition');
        engText = (engText) ? engText.innerText.trim() : '';
      } else {
        engSound = '';
        engText = '';
      }
      data['sample-sentence'].push({
        'table': i,
        'row': (((j+1)/3)-1), // 2 becomes 0, 5 becomes 1, 8 becomes 3, 11 becomes 4, 14 becomes 5
        'jpnText': jpnText,
        'jpnSound': jpnSound,
        'romaji': romaji,
        'engSound': engSound,
        'engText': engText
      }); 
    }
  }

  // get lesson transcript
  data['lesson-transcript'] = '';
  lesson_transcript_show_all_button = document.querySelector('.js-lsn3-toggle-lesson-transcript');
  if (lesson_transcript_show_all_button){
    lesson_transcript_show_all_button.click();
    data['lesson-transcript'] = (document.querySelector('#lesson-transcript')) ? document.querySelector('#lesson-transcript').innerText : '';
  }



  data = JSON.stringify(data);
  console.log(data);

  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      console.log(this.responseText);
      if (this.responseText === 'success') {
        pColor = '#66FFCC'; // green
        document.querySelector('.r101-headline__next').click()
      } else if (this.responseText === 'you already downloaded this lesson'){
        pColor = '#99CCFF'; // blue
        document.querySelector('.r101-headline__next').click()
      } else {
        pColor = '#FF9999'; // red
      }
      // let's just leave this in here for fun
      let paragraphs = document.getElementsByTagName('p');
      for (elt of paragraphs) {
        elt.style['background-color'] = pColor;
      }
    }
  });

  xhr.open("POST", "http://localhost:5000/submit");
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.setRequestHeader("Accept", "*/*");
  xhr.setRequestHeader("Cache-Control", "no-cache");
  xhr.setRequestHeader("Postman-Token", "a7f3cb3a-3bf3-4329-8626-3ab87e56dbdd,fcaf197b-1840-4923-bc1e-c2fe2e8bdfdd");
  xhr.setRequestHeader("cache-control", "no-cache");

  xhr.send(data);


}
