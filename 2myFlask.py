#!/usr/bin/python
# -*- coding: utf-8 -*-
import os, json, shutil
from flask import Flask, request
from pathlib import Path
from urllib.request import urlopen, urlretrieve
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello world'

@app.route('/submit', methods = ['POST'])
def submit():
    # set up project and get data
    project_root = '/home/brent/japanese/jpod101'
    data = request.get_json()
    # grab this info for dir structure
    season = data['season'].replace(" ", "_")
    lesson_no = data['lessonNo']
    lesson_dir = f"{project_root}/{season}/{lesson_no}/"
    json_file = lesson_dir + "lesson.json"
    my_file = Path(json_file)
    print('json_file: ' + json_file)
    print(str(my_file.is_file()))
    if my_file.is_file(): # this early exit is not working for some reason # it's fucking working now, no idea why
        return "you already downloaded this lesson"
    # create dir
    Path(lesson_dir).mkdir(parents=True, exist_ok=True)
    Path(lesson_dir + 'audio/').mkdir(parents=True, exist_ok=True)
    Path(lesson_dir + 'dialog/').mkdir(parents=True, exist_ok=True)
    Path(lesson_dir + 'vocab/').mkdir(parents=True, exist_ok=True)
    Path(lesson_dir + 'sample_sentences/').mkdir(parents=True, exist_ok=True)
    # make a season sample sentence folder
    season_sample_sentence_dir = f"{project_root}/{season}/Sample_Sentences/"
    Path(season_sample_sentence_dir).mkdir(parents=True, exist_ok=True)
    # make a season sample sentence folder
    season_vocab_dir = f"{project_root}/{season}/Vocab/"
    Path(season_vocab_dir).mkdir(parents=True, exist_ok=True)
    # write json to file
    f = open(json_file, "w")
    json.dump(data, f, ensure_ascii=False, indent=4)
    # clean up lesson transcript
    f = open(lesson_dir + 'transcript.txt', "w")
    transcript_list = data['lesson-transcript'].split('\n')
    clean_transcript = []
    for line in transcript_list:
        if line.isspace():
            continue
        else:
            clean_transcript.append(line.strip())
    clean_transcript = "\n\n".join(clean_transcript)
    f.write(clean_transcript)
    # get a machine-readable name for the audio files
    pathname = data['pathname']
    # download audio files
    machine_readable_lesson_name = pathname[:-1].split('/')[-1] # remove last char, split on /, use last list item
    for download in data['download-center']:
        # probably "Lesson_Audio", "Dialog", or "Review
        download_type = download['innerText'].replace(" ", "_")
        # I want to save the file in two places.  once in the lesson dir, and once in the "All download_type Files" folder for this season
        # save in the "All download_type" folder first 
        all_download_type_file = f"{project_root}/{season}/{download_type}/"
        Path(all_download_type_file).mkdir(parents=True, exist_ok=True)
        # onomatopoeia-1-getting-started_Lesson_Audio
        file_name = machine_readable_lesson_name + "_" + download_type + ".mp3"
        file_write_url = all_download_type_file + file_name
        # file_write_url = lesson_dir + 'audio/' + file_name
        print('begin urlretrieve ' + file_write_url)
        try:
            urlretrieve(download['href'], file_write_url)
            # now copy everything to lesson dir
            file_copy_url = lesson_dir + 'audio/' + download_type + '.mp3'
            shutil.copy(file_write_url, file_copy_url)
        except:
            print('failed to grab url')

    for dialog in data['dialogue']:
        if (dialog['sound'] == ""):
            print('skipping sentence with missing sound')
        elif (dialog['kanji'] == ""):
            print('skipping sentence with missing kanji')
        else:
            file_name = str(dialog['row']) + '_' + dialog['kanji'] + '.mp3'
            file_write_url = lesson_dir + 'dialog/' + file_name
            print('begin urlretrieve ' + file_write_url)
            try:
                urlretrieve(dialog['sound'], file_write_url)
            except:
                print('failed to grab url')
                continue

    for vocab in data['vocab']:
        if (vocab['audio'] == ""):
            print('skipping vocab with missing sound')
        audio_file = f"{vocab['row']}_0_{vocab['expression']}.mp3"
        slow_audio = f"{vocab['row']}_1_{vocab['expression']}.mp3"
        # write the files
        file_write_url = lesson_dir + 'vocab/' + audio_file
        print('begin urlretrieve ' + file_write_url)
        try:
            urlretrieve(vocab['audio'], file_write_url)
            # copy over to season vocab dir
            file_copy_url = season_vocab_dir + str(lesson_no) + '_' + audio_file
            print('filewrite: ' + file_write_url)
            print('filecopy:  ' + file_copy_url)
            shutil.copy(file_write_url, file_copy_url)
        except:
            print('failed to grab url')
            print(vocab['audio'])
            continue
        file_write_url = lesson_dir + 'vocab/' + slow_audio
        print('begin urlretrieve ' + file_write_url)
        try:
            urlretrieve(vocab['audio_breakdown'], file_write_url)
            # copy over to season vocab dir
            file_copy_url = season_vocab_dir + str(lesson_no) + '_' + slow_audio
            print('filewrite: ' + file_write_url)
            print('filecopy:  ' + file_copy_url)
            shutil.copy(file_write_url, file_copy_url)
        except:
            print('failed to grab url')
            print(vocab['audio_breakdown'])
            continue

    for sentence in data['sample-sentence']:
        file_name = f"{sentence['table']}_{sentence['row']}_{sentence['jpnText']}.mp3"
        file_write_url = lesson_dir + 'sample_sentences/' + file_name
        if sentence['jpnSound'] == "":
            print('skipping sample with missing data')
        else:
            print('begin urlretrieve ' + file_write_url)
            try:
                urlretrieve(sentence['jpnSound'], file_write_url)
                # now copy everything to lesson dir
                file_copy_url = season_sample_sentence_dir + str(lesson_no) + '_' + file_name
                print('filewrite: ' + file_write_url)
                print('filecopy:  ' + file_copy_url)
                shutil.copy(file_write_url, file_copy_url)
            except:
                print('failed to grab url')
    return 'success'







app.run(debug=True)
# app.run('host', 'port', debug=True)
